//
//  InstaBeanViewController.m
//  iBean
//
//  Created by Eddie Ehlin on 2012-12-28.
//  Copyright (c) 2012 Eddie Ehlin. All rights reserved.
//

#import "InstaBeanViewController.h"

@interface InstaBeanViewController ()

@end

@implementation InstaBeanViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  //Add any view related setup?
}

- (void)viewWillAppear:(BOOL)animated
{
  NSLog(@"InstaBean viewWillAppear");
  //[super viewWillAppear:<#animated#>];
  [self initInstaBean];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}


//-------------------------------------------------------------------
//                  UTILITY METHODS
//-------------------------------------------------------------------
- (void) updateExtractionSettingLabel {
  NSString *timerSetting = [NSString stringWithFormat:@"%1.0f", [self.extractionSettingStepper value]];
  self.extractionSettingLabel.text = timerSetting;
}

- (void) updateExtractionProgressLabel {
  self.extractionProgress += 0.1f;
  self.extractionProgressLabel.text = [NSString stringWithFormat:@"%1.1f", self.extractionProgress];
  
  if (self.extractionProgress >= self.extractionSettingStepper.value)
  {
    NSLog(@"Timer reached its limit!");
    [self haltExtractionTimer];
  }
}

- (void) haltExtractionTimer
{
  if (self.timer != nil)
  {
    [self.timer invalidate];
    self.timer = nil;
    [self.extractionButton setTitle:@"Start extraction" forState:UIControlStateNormal];
    self.extractionInProgress = NO;
    [self.extractionSettingStepper setEnabled:YES];
  }
}

//-------------------------------------------------------------------
//                  ACTION METHODS
//-------------------------------------------------------------------

- (void) initInstaBean
{
  NSLog(@"Loading & setting stepper!");
  //TODO: Read from storage..
  [self.extractionSettingStepper setValue:25.0f];
  [self updateExtractionSettingLabel];
}

- (IBAction)setExtractionTimer:(id)sender {
  [self updateExtractionSettingLabel];
}

- (IBAction)startExtraction:(id)sender {
  
  if (self.extractionInProgress == NO)
  {
    self.extractionInProgress = YES;
    self.extractionProgress = 0.0f;
    self.extractionProgressLabel.text = @"0.0";
    [self.extractionButton setTitle:@"Stop extraction" forState:UIControlStateNormal];
    [self.extractionSettingStepper setEnabled:NO];
    
    if (self.timer == nil)
    {
      self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(updateExtractionProgressLabel) userInfo:nil repeats:YES];
    }
    //TODO: Set red background image.
    
  }
  else
  {
    //TODO: Set green background image.
    [self haltExtractionTimer];
  }
}
@end
