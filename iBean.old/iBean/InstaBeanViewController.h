//
//  InstaBeanViewController.h
//  iBean
//
//  Created by Eddie Ehlin on 2012-12-28.
//  Copyright (c) 2012 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InstaBeanViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *extractionProgressLabel;
@property (weak, nonatomic) IBOutlet UIStepper *extractionSettingStepper;
@property (weak, nonatomic) IBOutlet UILabel *extractionSettingLabel;
@property (weak, nonatomic) IBOutlet UIButton *extractionButton;

@property (nonatomic) double extractionProgress;
@property (nonatomic) BOOL extractionInProgress;
@property (weak, nonatomic) NSTimer* timer;

//Utility methods
- (void) updateExtractionSettingLabel;
- (void) initInstaBean;
- (void) updateExtractionProgressLabel;
- (void) haltExtractionTimer;

//Event/signal based methods
- (IBAction)setExtractionTimer:(id)sender;
- (IBAction)startExtraction:(id)sender;

@end
