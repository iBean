//
//  AppDelegate.h
//  iBean
//
//  Created by Eddie Ehlin on 2012-12-28.
//  Copyright (c) 2012 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
