//
//  AppDelegate.h
//  iBean
//
//  Created by Eddie Ehlin on 2012-12-31.
//  Copyright (c) 2012 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) id timerCallbackTarget;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSMutableArray* activeThresholdsToProcess;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
