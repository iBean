//
//  ThresholdViewController.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-27.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "AppDelegate+Storage.h"
#import "ThresholdViewController.h"
#import "Threshold.h"
#import "Configuration+Interface.h"

@interface ThresholdViewController ()

@end

@implementation ThresholdViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self initViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*****************************************************
 Utility methods
 *****************************************************/
- (void) initViewController
{ 
  if (self.thresholdObjectIndex >= 0 && self.thresholdObjectIndex <= self.configuration.thresholds.count)
  {
    //We are editing an existing threshold!
    Threshold *tmpThreshold = [self.configuration.thresholds objectAtIndex:self.thresholdObjectIndex];
    self.thresholdNameTextField.text = tmpThreshold.name;
    self.thresholdEnabledSwitch.on = [tmpThreshold.enabled boolValue];
    self.thresholdValueStepper.value = [tmpThreshold.value doubleValue];
    self.thresholdRecurringSwitch.on = [tmpThreshold.recurring boolValue];
    [self valueStepperChanged:self];
  }
  
}

- (void) initWithConfigurationAndThresholdIndex: (Configuration*) configuration: (NSInteger) thresholdIndex;
{
  self.configuration = configuration;
  self.thresholdObjectIndex = thresholdIndex;
}

/*****************************************************
 UI Actions
 *****************************************************/
- (void) valueStepperChanged:(id)sender
{
  self.thresholdValueLabel.text = [[NSNumber numberWithDouble:self.thresholdValueStepper.value] stringValue];
}

- (void) commitThreshold:(id)sender
{
  Threshold* threshold = nil;
  NSError* commitError = nil;
  if (self.thresholdObjectIndex >= 0)
  {
    threshold = [self.configuration.thresholds objectAtIndex:self.thresholdObjectIndex];
  }
  else
  {
    threshold = [(AppDelegate*) [[UIApplication sharedApplication] delegate] createThreshold];
    [self.configuration addThresholdsObject:threshold];
  }
  
  if (threshold != nil)
  {
    threshold.name = self.thresholdNameTextField.text;
    threshold.value = [NSNumber numberWithDouble:self.thresholdValueStepper.value];
    threshold.enabled = [NSNumber numberWithBool:self.thresholdEnabledSwitch.on];
    threshold.recurring = [NSNumber numberWithBool:self.thresholdRecurringSwitch.on];
    
    //Commit changes and save succeeds then return to list of thresholds.
    commitError = [(AppDelegate*) [[UIApplication sharedApplication] delegate] save];
  }
  
  if (threshold == nil || commitError != nil)
  {
    //Rollback & present error!
    [(AppDelegate*) [[UIApplication sharedApplication] delegate] rollback];
    
    UIAlertView *commitThresholdAlert = [[UIAlertView alloc] initWithTitle:@"Save error!" message:@"iBean was unable to save threshold!\nPlease try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [commitThresholdAlert show];
  }
  else
  {
    [self.navigationController popViewControllerAnimated:YES];
  }
}


/*****************************************************
 Delegates
 *****************************************************/
//Purpose: When "return" is pressed the keyboard will go away and "change" event will be triggered.
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  //Return key makes the keyboard go away.
  if (textField == self.thresholdNameTextField)
  {
    [textField resignFirstResponder];
    return YES;
  }
  
  return NO;
}

@end
