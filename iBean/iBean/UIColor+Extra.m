//
//  UIColor+Extra.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-02-04.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "UIColor+Extra.h"

@implementation UIColor (Extra)

+ (UIColor*) colorFromHEX:(NSInteger)hex
{
  UIColor *c = [UIColor colorWithRed:((float)((hex & 0xFF0000) >> 16))/255.0
                                green:((float)((hex & 0xFF00) >> 8))/255.0
                                blue:((float)(hex & 0xFF))/255.0
                                alpha:1.0f];
  return c;
}

+ (NSInteger) selectedCellBackgroundColor
{
  //0xCBB6AC/*0xe8d1c5*/
  return 0xCBB6AC;
}

@end
