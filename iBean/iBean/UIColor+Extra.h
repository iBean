//
//  UIColor+Extra.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-02-04.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extra)

+(UIColor*) colorFromHEX: (NSInteger)hex;
+(NSInteger) selectedCellBackgroundColor;

@end
