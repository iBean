//
//  Bean.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-10.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "Bean.h"
#import "BeanCollection.h"


@implementation Bean

@dynamic amount;
@dynamic grindSetting;
@dynamic name;
@dynamic beanCollection;

@end
