//
//  ExtractionViewController.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-12.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BeanCollection;

@interface ExtractionViewController : UIViewController

@property (nonatomic, strong) BeanCollection *beanCollection;
@property (nonatomic, assign) BOOL editMode;

/* Utility methods */
- (void) initViewController;
- (void) initWithModeAndBeanCollection:(BOOL)editMode: (BeanCollection*) bc;
- (BOOL) commitBeanCollection;

/* UI Outlets */
@property (weak, nonatomic) IBOutlet UIStepper *extractionSettingStepper;
@property (weak, nonatomic) IBOutlet UILabel *extractionSettingLabel;

/* UI Actions */
- (IBAction) extractionSettingStepperChanged:(id)sender;

@end
