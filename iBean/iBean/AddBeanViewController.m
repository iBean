//
//  AddBeanViewController.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-09.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "AppDelegate+Storage.h"
#import "AddBeanViewController.h"
#import "Bean+Interface.h"
#import "BeanCollection+Interface.h"

@interface AddBeanViewController ()

@end

@implementation AddBeanViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self initViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*****************************************************
 Utility methods
 *****************************************************/
- (void) initViewController
{
  //If there exists beans in collection, then pre set the
  //grinder setting to the value of latest added bean.
  if (self.beanCollection != nil)
  {
    if (self.beanCollection.beans.count > 0)
    {
      NSNumber *grinderSetting = ((Bean*)[self.beanCollection.beans lastObject]).grindSetting;
      self.grinderSettingLabel.text = [NSString stringWithFormat:@"%1.0f", [grinderSetting doubleValue]];
      self.grinderSettingStepper.value = [grinderSetting doubleValue];
    }
  }
}

- (void) initWithModeAndBeanCollection:(BOOL)editMode: (BeanCollection*) bc
{
  self.editMode = editMode;
  self.beanCollection = bc;
}

/*****************************************************
 UI Actions
 *****************************************************/
#pragma mark - IBActions
- (void) amountStepperChanged:(id)sender
{
  self.amountLabel.text = [NSString stringWithFormat:@"%1.1f", self.amountStepper.value];
}

- (void) grinderSettingStepperChanged:(id)sender
{
  self.grinderSettingLabel.text = [NSString stringWithFormat:@"%1.0f", self.grinderSettingStepper.value];
}

- (void) cancelBean:(id)sender
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) commitBean:(id)sender
{
  Bean *b = [(AppDelegate*) [[UIApplication sharedApplication] delegate] createBean];
  if (b != nil && self.beanCollection != nil)
  {
    b.amount = [NSNumber numberWithDouble:self.amountStepper.value];
    b.name = self.beanNameTextField.text;
    b.grindSetting = [NSNumber numberWithDouble:self.grinderSettingStepper.value];
    [self.beanCollection addBeansObject:b];
    
    //Go back to the bean list.
    [self dismissViewControllerAnimated:YES completion:nil];
  }
  else
  {
    UIAlertView *createErrorAlert = [[UIAlertView alloc]
                                     initWithTitle:@"Create bean error!"
                                     message:@"Unable to create a new bean.\n(Please check available memory and try again.)"
                                     delegate:nil
                                     cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [createErrorAlert show];
  }
}

/*****************************************************
 Delegate
 *****************************************************/
#pragma mark - UITextFieldDelegate methods
- (void) textFieldDidBeginEditing:(UITextField *)textField
{
  if (textField == self.beanNameTextField)
  {
    [[[self.addBeanNavigationBar.items objectAtIndex:0] rightBarButtonItem] setEnabled:NO];
  }
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
  if (textField == self.beanNameTextField)
  {
    [[[self.addBeanNavigationBar.items objectAtIndex:0] rightBarButtonItem] setEnabled:YES];
  }
}

//Purpose: When "return" is pressed the keyboard will go away and "change" event will be triggered.
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  //Return key makes the keyboard go away.
  if (textField == self.beanNameTextField)
  {
    [textField resignFirstResponder];
    return YES;
  }
  
  return NO;
}
@end
