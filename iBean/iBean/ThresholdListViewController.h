//
//  ThresholdListViewController.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-27.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Configuration;

@interface ThresholdListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) Configuration* configuration;
@property (nonatomic, strong) UIBarButtonItem *thresholdListEditDoneButton;

/* Utility methods */
- (void) initViewController;
- (void) initWithConfiguration: (Configuration*) configuration;

/* UI Outlets */
@property (weak, nonatomic) IBOutlet UITableView *thresholdListTableView;
@property (weak, nonatomic) IBOutlet UIToolbar *thresholdListBottomToolbar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *thresholdListEditButton;

/* UI Actions */
- (IBAction)toggleEditMode:(id)sender;

@end
