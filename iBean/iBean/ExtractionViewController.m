//
//  ExtractionViewController.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-12.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "ExtractionViewController.h"
#import "BeanCollection+Interface.h"
#import "AppDelegate+Storage.h"

@interface ExtractionViewController ()

@end

@implementation ExtractionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self initViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
  //Check if we should perform the segue back to bean collection list.
  if ([identifier isEqualToString:@"CommitBeanCollectionSegue"])
  {
    return [self commitBeanCollection];
  }
  
  return YES;
}

/*****************************************************
 Utility methods
 *****************************************************/
- (void) initViewController
{
  //No need to check self.editMode here, since bean collection
  //receives a default value of 25.0 for extraction time.
  if (self.beanCollection != nil)
  {
    self.extractionSettingLabel.text = [NSString stringWithFormat:@"%1.1f", [self.beanCollection.extractionTime doubleValue]];
    self.extractionSettingStepper.value = [self.beanCollection.extractionTime doubleValue];
  }
}

- (void) initWithModeAndBeanCollection:(BOOL)editMode: (BeanCollection*) bc
{
  self.editMode = editMode;
  self.beanCollection = bc;
}

- (BOOL) commitBeanCollection
{
  NSError *commitError = nil;
  commitError = [(AppDelegate*) [[UIApplication sharedApplication] delegate] save];
  
  if (commitError != nil)
  {
    UIAlertView *commitErrorAlert = [[UIAlertView alloc]
                                     initWithTitle:@"Save error!"
                                     message:[commitError localizedDescription]
                                     delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [commitErrorAlert show];
    return NO;
  }
  
  //Bean collection successfully saved, lets go back to list of bean collections.
  return YES;
}

/*****************************************************
 UI Actions
 *****************************************************/
- (void) extractionSettingStepperChanged:(id)sender
{
  self.extractionSettingLabel.text = [NSString stringWithFormat:@"%1.1f", self.extractionSettingStepper.value];
  if (self.beanCollection != nil)
  {
    self.beanCollection.extractionTime = [NSNumber numberWithDouble:self.extractionSettingStepper.value];
  }
}

@end
