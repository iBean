//
//  BeanCollection+Interface.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-10.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "BeanCollection.h"

@interface BeanCollection (Interface)

- (void) addBeansObject: (Bean *)b;
- (void) removeObjectFromBeansAtIndex:(NSUInteger)beanIndex; 
- (void)replaceBeansAtIndexes:(NSIndexSet *)beanIndexes withBeans:(NSArray *)beans;
@end
