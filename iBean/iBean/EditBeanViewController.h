//
//  EditBeanViewController.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-12.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//
@class BeanCollection;

@interface EditBeanViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) BeanCollection *beanCollection;
@property (nonatomic, assign) BOOL editMode;
@property (nonatomic, assign) NSUInteger editBeanIndex;

/* Utility methods */
- (void) initWithModeAndBeanCollectionAndBeanIndex:(BOOL)editMode: (BeanCollection*)bc: (NSUInteger) beanIndex;
- (void) initViewController;

/* UI Outlets */
@property (weak, nonatomic) IBOutlet UITextField *beanNameTextField;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIStepper *amountStepper;
@property (weak, nonatomic) IBOutlet UILabel *grinderSettingLabel;
@property (weak, nonatomic) IBOutlet UIStepper *grinderSettingStepper;


/* UI Actions */
- (IBAction) amountStepperChanged:(id)sender;
- (IBAction) grinderSettingStepperChanged:(id)sender;
- (IBAction) commitBean:(id)sender;

@end
