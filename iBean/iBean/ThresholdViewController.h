//
//  ThresholdViewController.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-27.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Threshold;
@class Configuration;

@interface ThresholdViewController : UITableViewController <UITextFieldDelegate>

@property (nonatomic, assign) NSInteger thresholdObjectIndex;
@property (nonatomic, strong) Configuration* configuration;

/* Utility methods */
- (void) initViewController;
- (void) initWithConfigurationAndThresholdIndex: (Configuration*) configuration: (NSInteger) thresholdIndex;

/* UI Outlets */
@property (weak, nonatomic) IBOutlet UITextField *thresholdNameTextField;
@property (weak, nonatomic) IBOutlet UILabel *thresholdValueLabel;
@property (weak, nonatomic) IBOutlet UIStepper *thresholdValueStepper;
@property (weak, nonatomic) IBOutlet UISwitch *thresholdEnabledSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *thresholdRecurringSwitch;

/* UI Actions */
- (IBAction)valueStepperChanged:(id)sender;
- (IBAction)commitThreshold:(id)sender;

/* UITextFieldDelegate */
- (BOOL)textFieldShouldReturn:(UITextField *)textField;

@end
