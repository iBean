//
//  BeanCollectionBeanListViewController.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-05.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BeanCollection;

@interface BeanCollectionBeanListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) BeanCollection *beanCollection;
@property (nonatomic, assign) BOOL editMode;
@property (nonatomic, strong) UIBarButtonItem *beanListEditDoneButton;

/* Utility methods */
- (void) initViewController;
- (void) initWithModeAndBeanCollection:(BOOL)editMode: (BeanCollection*) bc;

/* UI Outlets */
@property (weak, nonatomic) IBOutlet UITableView *beanListTableView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *beanListEditButton;
@property (weak, nonatomic) IBOutlet UIToolbar *beanListBottomToolbar;


/* UI Actions */
- (IBAction) toggleEditMode:(id)sender;

@end
