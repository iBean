//
//  Threshold.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-02-17.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "Threshold.h"
#import "Configuration.h"


@implementation Threshold

@dynamic enabled;
@dynamic name;
@dynamic value;
@dynamic recurring;
@dynamic configuration;

@end
