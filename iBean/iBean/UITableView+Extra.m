//
//  UITableView+Extra.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-02-06.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "UITableView+Extra.h"
#import <QuartzCore/CoreAnimation.h>

@implementation UITableView (Extra)

+ (UIView*) bottomShadowForTableView:(UITableView *)tableView
{
  UIView *shadowView = nil;
  
  if (tableView != nil)
  {
    UIColor *blackColor = [[UIColor blackColor] colorWithAlphaComponent:0.6f];
    UIColor *lightColor = [UIColor clearColor];
    NSInteger shadowHeight = 10;
    
    shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, shadowHeight)];
    shadowView.alpha = 0.6f;
    
    CAGradientLayer *shadowGradient = [[CAGradientLayer alloc] init];
    shadowGradient.frame = CGRectMake(0,0, tableView.frame.size.width, shadowHeight);
    shadowGradient.colors = [NSArray arrayWithObjects:(id)[blackColor CGColor], (id)[lightColor CGColor], nil];

    [shadowView.layer addSublayer:shadowGradient];
  }
  return shadowView;
}

@end
