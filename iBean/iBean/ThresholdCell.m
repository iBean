//
//  ThresholdCell.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-27.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "ThresholdCell.h"
#import "Threshold.h"
#import "AppDelegate+Storage.h"

@implementation ThresholdCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/*****************************************************
 Utility methods
 *****************************************************/
- (void) initWithThresholdEntity:(Threshold *)threshold
{
  self.threshold = threshold;
  if (self.threshold != nil)
  {
    self.nameLabel.text = self.threshold.name;
    self.valueLabel.text = [self.threshold.value stringValue];
    if ([self.threshold.recurring boolValue] == YES)
    {
      self.valueLabel.text = [self.valueLabel.text stringByAppendingString:@" (recurring)"];
    }
    self.enabledSwitch.on = [self.threshold.enabled boolValue];
  }
  
  //Setup UI action
  [self.enabledSwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
}

/*****************************************************
 UI Actions
 *****************************************************/
- (void) switchChanged:(id)sender
{
  if (self.threshold != nil)
  {
    self.threshold.enabled = [NSNumber numberWithBool:self.enabledSwitch.on];
    NSError* error = [(AppDelegate*) [[UIApplication sharedApplication] delegate] save];
    if (error != nil)
    {
      //Rollback
      [(AppDelegate*) [[UIApplication sharedApplication] delegate] rollback];
      
      UIAlertView *switchChangedSaveAlert = [[UIAlertView alloc]
                                             initWithTitle:@"Save error!"
                                             message:@"iBean was unable to save threshold state!\nPlease try again."
                                             delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles: nil];
      [switchChangedSaveAlert show];

      #warning TODO - Make sure that rollback resets the value back to previous one!!!!!
      self.enabledSwitch.on = [self.threshold.enabled boolValue];
    }
  }
}

@end
