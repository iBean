//
//  BeanCollectionExtractionViewController.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-18.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BeanCollection;

@interface BeanCollectionExtractionViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>

@property (nonatomic, strong) BeanCollection *beanCollection;
@property (nonatomic, assign) double extractionProgress;
@property (nonatomic, assign) BOOL extractionInProgress;

/* Utility methods */
- (void) initViewController;
- (void) initWithBeanCollection:(BeanCollection*) bc;
- (void) updateExtractionProgress;
- (void) haltExtractionTimer;
- (void) timerInterrupted;

/* UI Outlets */
@property (weak, nonatomic) IBOutlet UITableView *beanTableView;
@property (weak, nonatomic) IBOutlet UITextView *beanCollectionNoteTextView;
@property (weak, nonatomic) IBOutlet UILabel *extractionProgressLabel;
@property (weak, nonatomic) IBOutlet UIButton *extractionButton;
@property (weak, nonatomic) IBOutlet UIView *extractionTimerView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *toggleExtractionTimerButton;

/* UI Actions */
- (IBAction) startExtraction:(id)sender;
- (IBAction) showExtractionTimer:(id)sender;
@end
