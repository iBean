//
//  Configuration+Interface.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-14.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "Configuration.h"

@interface Configuration (Interface)

- (void) addThresholdsObject: (Threshold *)t;
- (void) removeObjectFromThresholdsAtIndex:(NSUInteger)thresholdIndex;
- (void)replaceThresholdsAtIndexes:(NSIndexSet *)thresholdIndexes withThresholds:(NSArray *)thresholds;

@end
