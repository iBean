//
//  InstantExtractionViewController.h
//  iBean
//
//  Created by Eddie Ehlin on 2012-12-31.
//  Copyright (c) 2012 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InstantExtractionViewController : UIViewController <UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) double extractionProgress;
@property (nonatomic, assign) BOOL extractionInProgress;
@property (nonatomic, strong) NSMutableArray *extractionTimes;

/* Utility methods */
- (void) initViewController;
- (void) updateExtractionSettingLabel;
- (void) updateExtractionProgressLabel;
- (void) haltExtractionTimer;
- (void) timerInterrupted;

/* UI Outlets */
@property (weak, nonatomic) IBOutlet UILabel *extractionSettingLabel;
@property (weak, nonatomic) IBOutlet UIStepper *extractionSettingStepper;
@property (weak, nonatomic) IBOutlet UILabel *extractionProgressLabel;
@property (weak, nonatomic) IBOutlet UILabel *extractionStateLabel;
@property (weak, nonatomic) IBOutlet UITableView *extractionTimesTableView;

/* UI Actions */
- (IBAction)startExtraction:(id)sender;
- (IBAction)setExtractionTimer:(id)sender;

@end
