//
//  Configuration.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-27.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Threshold;

@interface Configuration : NSManagedObject

@property (nonatomic, retain) NSNumber * instantExtractionTimer;
@property (nonatomic, retain) NSNumber * extractionCount;
@property (nonatomic, retain) NSNumber * countExtractions;
@property (nonatomic, retain) NSNumber * useThresholds;
@property (nonatomic, retain) NSOrderedSet *thresholds;
@end

@interface Configuration (CoreDataGeneratedAccessors)

- (void)insertObject:(Threshold *)value inThresholdsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromThresholdsAtIndex:(NSUInteger)idx;
- (void)insertThresholds:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeThresholdsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInThresholdsAtIndex:(NSUInteger)idx withObject:(Threshold *)value;
- (void)replaceThresholdsAtIndexes:(NSIndexSet *)indexes withThresholds:(NSArray *)values;
- (void)addThresholdsObject:(Threshold *)value;
- (void)removeThresholdsObject:(Threshold *)value;
- (void)addThresholds:(NSOrderedSet *)values;
- (void)removeThresholds:(NSOrderedSet *)values;
@end
