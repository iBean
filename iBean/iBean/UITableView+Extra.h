//
//  UITableView+Extra.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-02-06.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Extra)

+ (UIView*)bottomShadowForTableView:(UITableView*) tableView;

@end
