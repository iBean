//
//  BeanCollectionExtractionViewController.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-18.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//
#import <AudioToolbox/AudioToolbox.h>
#import "BeanCollectionExtractionViewController.h"
#import "BeanCollection+Interface.h"
#import "BeanCell.h"
#import "Bean+Interface.h"
#import "AppDelegate+Storage.h"
#import "UITableView+Extra.h"
#import "UIColor+Extra.h"
#import <QuartzCore/QuartzCore.h>

@interface BeanCollectionExtractionViewController ()

@end

@implementation BeanCollectionExtractionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
  
  //Bring the extraction timer view on top
  [self.view bringSubviewToFront:self.extractionTimerView];
  
  //Set row height for bean table view
  [self.beanTableView setRowHeight:60.0f];
  
  //[self.beanCollectionNoteTextView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed: @"textview-bg.png"]]];
}

- (void) viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  [self haltExtractionTimer];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self initViewController];
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*****************************************************
 Utility methods
 *****************************************************/
#pragma mark - Utility methods
- (void) initViewController
{
  if (self.beanCollection != nil)
  {
    self.navigationItem.title = self.beanCollection.name;
    self.extractionProgressLabel.text = [NSString stringWithFormat:@"%1.1f", [self.beanCollection.extractionTime doubleValue]];
    
    //Make sure that extraction button always is non-toggled when entering the view.
    self.toggleExtractionTimerButton.style = UIBarButtonItemStylePlain;
    
    if (![self.beanCollection.note isEqualToString:@""])
    {
      self.beanCollectionNoteTextView.text = self.beanCollection.note;
    }
    else
    {
      [self.beanCollectionNoteTextView setHidden:YES];
    }
  }
  else
  {
    NSLog(@"BeanCollectionExtractionViewController - initViewController: Unable to init view controller without beanCollection (beanCollection is nil!)");
  }
}

- (void) initWithBeanCollection:(BeanCollection *)bc
{
  self.beanCollection = bc;
}

- (void) updateExtractionProgress
{
  self.extractionProgress -= 0.1f;
  self.extractionProgressLabel.text = [NSString stringWithFormat:@"%1.1f", self.extractionProgress];
  
  if (self.extractionProgress <= 0.0f)
  {
    [self haltExtractionTimer];
    
    UIAlertView *extractionCompleteAlert = [[UIAlertView alloc]
                                            initWithTitle:@"Coffee time!"
                                            message:@"Extraction completed."
                                            delegate:self
                                            cancelButtonTitle:@"OK" otherButtonTitles: nil];
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    [extractionCompleteAlert show];
  }
}

- (void) haltExtractionTimer
{
  if ([(AppDelegate*) [[UIApplication sharedApplication] delegate] getTimer] != nil)
  {
    [(AppDelegate*) [[UIApplication sharedApplication] delegate] haltTimer:YES];
    
    //Return UI to "Start extraction" state.
    [self timerInterrupted];
  }
}

- (void) timerInterrupted
{
  //Return UI to "Start extraction" state.
  [self.extractionButton setTitle:@"Start" forState:UIControlStateNormal];
  self.extractionProgressLabel.text = [NSString stringWithFormat:@"%1.1f", [self.beanCollection.extractionTime doubleValue]];
  self.extractionInProgress = NO;
}

/*****************************************************
 UI Actions
 *****************************************************/
#pragma mark - UI action methods

- (void) startExtraction:(id)sender
{
  //Extraction in progress?
  if ([(AppDelegate*) [[UIApplication sharedApplication] delegate] getTimer] == nil || self.extractionInProgress == NO)
  {
    self.extractionProgress = [self.beanCollection.extractionTime doubleValue];
    [self.extractionButton setTitle:@"Stop" forState:UIControlStateNormal];
    self.extractionInProgress = YES;
    
    [(AppDelegate*) [[UIApplication sharedApplication] delegate] createTimer:self :@selector(updateExtractionProgress) :nil :YES];
  }
  else if ([(AppDelegate*) [[UIApplication sharedApplication] delegate] getTimer] != nil)
  {
    [self haltExtractionTimer];
  }
}

- (void) showExtractionTimer:(id)sender
{
  [UIView animateWithDuration:0.2f delay: 0.0f options: UIViewAnimationOptionCurveEaseInOut
                   animations:^{
                     if (self.extractionTimerView.frame.origin.y < 0)
                     {
                       self.toggleExtractionTimerButton.style = UIBarButtonItemStyleDone;
                       //Slide in
                       self.extractionTimerView.frame = CGRectMake(
                                                                   self.extractionTimerView.frame.origin.x,
                                                                   0.0,
                                                                   self.extractionTimerView.frame.size.width,
                                                                   self.extractionTimerView.frame.size.height);
                       
                       //Drop shadow
                       [self.extractionTimerView.layer setShadowColor:[UIColor blackColor].CGColor];
                       [self.extractionTimerView.layer setShadowOpacity:0.8];
                       [self.extractionTimerView.layer setShadowRadius:3.0];
                       [self.extractionTimerView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
                     }
                     else
                     {
                       self.toggleExtractionTimerButton.style = UIBarButtonItemStylePlain;
                       //Slide out
                       self.extractionTimerView.frame = CGRectMake(
                                                                   self.extractionTimerView.frame.origin.x,
                                                                   self.extractionTimerView.frame.size.height * -1,
                                                                   self.extractionTimerView.frame.size.width,
                                                                   self.extractionTimerView.frame.size.height);
                       //Remove shadow
                       [self.extractionTimerView.layer setShadowRadius:0.0];
                       [self.extractionTimerView.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
                     }
                   }
                   completion:^(BOOL finished){
                   }
   ];
}

/*****************************************************
 Delegates
 *****************************************************/
#pragma mark - UITableViewDelegate & datasource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return self.beanCollection.beans.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"BeanCell";
  BeanCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  
  //Configure the cell and set its title
  Bean *currentBean = [self.beanCollection.beans objectAtIndex:indexPath.row];
  
  [cell initWithNameGrindSettingAndAmount:currentBean.name :currentBean.grindSetting :currentBean.amount];
  
  return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
  //Alternate the background color
  [cell.contentView setBackgroundColor: [UIColor colorFromHEX:(indexPath.row % 2 == 0) ? 0xFFFFFF : /*[UIColor selectedCellBackgroundColor]*/0xe9e5df]];
}

- (UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
  //Add shadow to the bottom of the bean table view.
  return [UITableView bottomShadowForTableView:self.beanTableView];
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
  return 10.0f;
}

#pragma mark - UIAlertView delegate
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
  //This will process thresholds etc.
  [(AppDelegate*) [[UIApplication sharedApplication] delegate] incrementExtractionCount];
}

@end
