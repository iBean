//
//  EditBeanViewController.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-12.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "BeanCollection+Interface.h"
#import "Bean+Interface.h"
#import "EditBeanViewController.h"

@interface EditBeanViewController ()

@end

@implementation EditBeanViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self initViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*****************************************************
 Utility methods
 *****************************************************/
- (void) initWithModeAndBeanCollectionAndBeanIndex:(BOOL)editMode: (BeanCollection*) bc: (NSUInteger) beanIndex
{
  self.editMode = editMode;
  self.beanCollection = bc;
  self.editBeanIndex = beanIndex;
  if (bc == nil)
  {
    NSLog(@"EditBeanViewController - initWithModeAndBeanCollectionAndBeanIndex: Received bogus bean collection (nil)!");
#warning TODO - This should never occur!
  }
}

- (void) initViewController
{
  //Fetch bean data from the collection and populate the ui.
  Bean *b = [self.beanCollection.beans objectAtIndex:self.editBeanIndex];
  if (b != nil)
  {
    self.beanNameTextField.text = b.name;
    self.amountLabel.text = [NSString stringWithFormat:@"%1.1f", [b.amount doubleValue]];
    self.amountStepper.value = [b.amount doubleValue];
    self.grinderSettingLabel.text = [NSString stringWithFormat:@"%1.0f", [b.grindSetting doubleValue]];
    self.grinderSettingStepper.value = [b.grindSetting doubleValue];
  }
}

/*****************************************************
 UI Actions
 *****************************************************/
#pragma mark - IBActions
- (void) amountStepperChanged:(id)sender
{
  self.amountLabel.text = [NSString stringWithFormat:@"%1.1f", self.amountStepper.value];
}

- (void) grinderSettingStepperChanged:(id)sender
{
  self.grinderSettingLabel.text = [NSString stringWithFormat:@"%1.0f", self.grinderSettingStepper.value];
}

- (void) commitBean:(id)sender
{
  if (self.beanCollection != nil)
  {
    Bean *b = [self.beanCollection.beans objectAtIndex:self.editBeanIndex];
    if (b != nil)
    {
      b.name = self.beanNameTextField.text;
      b.amount = [NSNumber numberWithDouble:self.amountStepper.value];
      b.grindSetting = [NSNumber numberWithDouble:self.grinderSettingStepper.value];
      
      //Lets return to the list
      [[self navigationController] popViewControllerAnimated:YES];
    }
  }
}

/*****************************************************
 Delegate
 *****************************************************/
#pragma mark - UITextFieldDelegate methods
- (void) textFieldDidBeginEditing:(UITextField *)textField
{
  if (textField == self.beanNameTextField)
  {
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
  }
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
  if (textField == self.beanNameTextField)
  {
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
  }
}

//Purpose: When "return" is pressed the keyboard will go away and "change" event will be triggered.
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  //Return key makes the keyboard go away.
  if (textField == self.beanNameTextField)
  {
    [textField resignFirstResponder];
    return YES;
  }
  
  return NO;
}

@end
