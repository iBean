//
//  BeanCollection.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-10.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "BeanCollection.h"
#import "Bean.h"


@implementation BeanCollection

@dynamic created;
@dynamic extractionCount;
@dynamic extractionTime;
@dynamic modified;
@dynamic name;
@dynamic note;
@dynamic beans;

@end
