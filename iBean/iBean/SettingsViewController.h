//
//  SettingsViewController.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-27.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Configuration;

@interface SettingsViewController : UITableViewController <UIAlertViewDelegate>

@property (nonatomic, strong) Configuration *configuration;

/* Utility methods */
- (void) initViewController;

/* UI Outlets */
@property (weak, nonatomic) IBOutlet UISwitch *countExtractionsSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *useThresholdsSwitch;
@property (weak, nonatomic) IBOutlet UIStepper *extractionCountStepper;
@property (weak, nonatomic) IBOutlet UILabel *extractionCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *resetExtractionCountButton;

/* UI Actions */
- (IBAction)countExtractionsSwitchChanged:(id)sender;
- (IBAction)useThresholdsSwitchChanged:(id)sender;
- (IBAction)extractionCountStepperChanged:(id)sender;
- (IBAction)resetExtractionCount:(id)sender;

@end
