//
//  BeanCell.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-18.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeanCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *beanImageLeft;
@property (weak, nonatomic) IBOutlet UILabel *beanNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *beanGrindSettingLabel;
@property (weak, nonatomic) IBOutlet UIImageView *beanImageRight;
@property (weak, nonatomic) IBOutlet UILabel *beanAmountLabel;


- (void) initWithNameGrindSettingAndAmount: (NSString*) name: (NSNumber*)grindSetting: (NSNumber*) amount;
@end
