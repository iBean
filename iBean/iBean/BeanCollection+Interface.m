//
//  BeanCollection+Interface.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-10.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "BeanCollection+Interface.h"

@implementation BeanCollection (Interface)

- (void) addBeansObject:(Bean *)b
{
  NSMutableOrderedSet *tmpSet = [self mutableOrderedSetValueForKey:@"beans"];
  [tmpSet addObject:b];
}

- (void) removeObjectFromBeansAtIndex:(NSUInteger)beanIndex
{
  /*
   NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
   [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"beans"];
   NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self mutableOrderedSetValueForKey:@"beans"]];
   [tmpOrderedSet removeObjectAtIndex:idx];
   [self setPrimitiveValue:tmpOrderedSet forKey:@"beans"];
   [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"beans"];
   */
  
  NSMutableOrderedSet *tmpSet = [self mutableOrderedSetValueForKey:@"beans"];
  if (beanIndex >= 0 && beanIndex < tmpSet.count)
    [tmpSet removeObjectAtIndex:beanIndex];
  else
    NSLog(@"BeanCollection+Interface - removeBeansObjectAtIndex: Index out of range!");
}

- (void)replaceBeansAtIndexes:(NSIndexSet *)beanIndexes withBeans:(NSArray *)beans
{
  /*
  [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"beans"];
  NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self mutableOrderedSetValueForKey:@"beans"]];
  [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:values];
  [self setPrimitiveValue:tmpOrderedSet forKey:@"beans"];
  [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"beans"];
  */
  
  if (beans.count > 0)
  {
    NSMutableOrderedSet *tmpSet = [self mutableOrderedSetValueForKey:@"beans"];
    [tmpSet replaceObjectsAtIndexes:beanIndexes withObjects:beans];
  }
  
}


@end
