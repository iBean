//
//  BeanCell.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-18.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "BeanCell.h"

@implementation BeanCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) initWithNameGrindSettingAndAmount: (NSString*) name: (NSNumber*)grindSetting: (NSNumber*) amount
{
  self.beanNameLabel.text = name;
  self.beanGrindSettingLabel.text = [NSString stringWithFormat:@"Grind @ %@", grindSetting];
  self.beanAmountLabel.text = [NSString stringWithFormat:@"%1.1f g", [amount doubleValue]];
}

@end
