//
//  BeanCollection.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-10.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Bean;

@interface BeanCollection : NSManagedObject

@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSNumber * extractionCount;
@property (nonatomic, retain) NSNumber * extractionTime;
@property (nonatomic, retain) NSDate * modified;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSOrderedSet *beans;
@end

@interface BeanCollection (CoreDataGeneratedAccessors)

- (void)insertObject:(Bean *)value inBeansAtIndex:(NSUInteger)idx;
- (void)removeObjectFromBeansAtIndex:(NSUInteger)idx;
- (void)insertBeans:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeBeansAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInBeansAtIndex:(NSUInteger)idx withObject:(Bean *)value;
- (void)replaceBeansAtIndexes:(NSIndexSet *)indexes withBeans:(NSArray *)values;
- (void)addBeansObject:(Bean *)value;
- (void)removeBeansObject:(Bean *)value;
- (void)addBeans:(NSOrderedSet *)values;
- (void)removeBeans:(NSOrderedSet *)values;
@end
