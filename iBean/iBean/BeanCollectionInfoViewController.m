//
//  BeanCollectionInfoViewController.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-05.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "BeanCollectionInfoViewController.h"
#import "BeanCollectionBeanListViewController.h"
#import "AppDelegate+Storage.h"
#import "BeanCollection.h"
#import <QuartzCore/QuartzCore.h>

@interface BeanCollectionInfoViewController ()

@end

@implementation BeanCollectionInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
	// Do any additional setup after loading the view.
  
  //Style the UITextView to look like a UITextField
  self.beanCollectionNoteTextView.layer.cornerRadius = 6.0f;
  self.beanCollectionNoteTextViewShadowView.layer.cornerRadius = self.beanCollectionNoteTextView.layer.cornerRadius;
  self.beanCollectionNoteTextViewShadowView.layer.shadowOpacity = 0.8f;
  self.beanCollectionNoteTextViewShadowView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
  self.beanCollectionNoteTextViewShadowView.layer.shadowOffset = CGSizeMake(0.0f, -2.5f);
  self.beanCollectionNoteTextViewShadowView.layer.shadowRadius = 0.9f;
}

- (void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self initViewController];
  
  //Register keyboard events
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(keyboardDisplayed:)
                                               name:UIKeyboardDidShowNotification object:nil];
  
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(keyboardHidden:)
                                               name:UIKeyboardWillHideNotification object:nil];
}

- (void) viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  
  //Unregister keyboard events
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  //BeanInfoBeanListSegue
  if ([segue.identifier isEqualToString:@"BeanInfoBeanListSegue"])
  {
    //Pass on the edit/add state and bean collection
    BeanCollectionBeanListViewController *beanListViewController = segue.destinationViewController;
    [beanListViewController initWithModeAndBeanCollection:self.editMode :self.beanCollection];
  }
  else if ([segue.identifier isEqualToString:@"BeanCollectionInfoCancelSegue"])
  {
    //Wipe all changes!
    [(AppDelegate*) [[UIApplication sharedApplication] delegate] rollback];
  }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
  
  if ([identifier isEqualToString:@"BeanInfoBeanListSegue"])
  {
    //Validate data (for now just tranfer UI data to the bean collection data structure)
    if (self.beanCollection != nil)
    {
      self.beanCollection.name = self.beanCollectionNameTextField.text;
      self.beanCollection.note = self.beanCollectionNoteTextView.text;
    }
  }
  
  return YES;
}

/*****************************************************
 Utility methods
 *****************************************************/
#pragma mark - Utility methods
- (void) initViewController
{
  if (self.editMode == YES)
  {
    if (self.beanCollection != nil)
    {
      //Set values from bean collection structure.
      self.beanCollectionNameTextField.text = self.beanCollection.name;
      self.beanCollectionNoteTextView.text = self.beanCollection.note;
    }
    else
    {
#warning TODO - This should never occur!?
      NSLog(@"BeanCollectionInfoViewController - initViewController in edit state, but bean collection is null!");
    }
  }
  
  //Allocate text view&field done button, which will replace Next button when needed.
  self.beanCollectionTextViewDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self.beanCollectionNoteTextView action:@selector(resignFirstResponder)];
  self.beanCollectionTextFieldDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self.beanCollectionNameTextField action:@selector(resignFirstResponder)];  
}

- (void) initWithModeAndBeanCollection:(BOOL)editMode :(BeanCollection *)bc
{
  self.editMode = editMode;
  self.beanCollection = bc;
}

/*****************************************************
 UI Actions
 *****************************************************/
#pragma mark - IBActions
- (void) keyboardDisplayed:(NSNotification *)keyboardNotification
{ 
  //Make sure that beanCollectionTextView is accessable even with keyboard visible.
  //See: https://developer.apple.com/library/ios/#documentation/StringsTextFonts/Conceptual/TextAndWebiPhoneOS/KeyboardManagement/KeyboardManagement.html
  NSDictionary* info = [keyboardNotification userInfo];
  CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
  
  UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
  self.beanCollectionScrollView.contentInset = contentInsets;
  self.beanCollectionScrollView.scrollIndicatorInsets = contentInsets;
  
  // If active text field is hidden by keyboard, scroll it so it's visible
  // Your application might not need or want this behavior.
  /*
  CGRect aRect = self.view.frame;
  aRect.size.height -= kbSize.height;
  if (!CGRectContainsPoint(aRect, self.beanCollectionNoteTextView.frame.origin) ) {
    CGPoint scrollPoint = CGPointMake(0.0, self.beanCollectionNoteTextView.frame.origin.y-kbSize.height);
    [self.beanCollectionScrollView setContentOffset:scrollPoint animated:YES];
  }*/
}

- (void) keyboardHidden:(NSNotification *)keyboardNotification
{
  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
  self.beanCollectionScrollView.contentInset = contentInsets;
  self.beanCollectionScrollView.scrollIndicatorInsets = contentInsets;
}

- (void) cancelBeanCollection:(id)sender
{
  UIAlertView *confirmCancel = [[UIAlertView alloc]
                                initWithTitle:self.editMode ? @"Cancel changes?" : @"Cancel bean collection?"
                                message:self.editMode ? @"Are you sure you want to cancel?\n(Changes entered will be lost)" : @"Are you sure you want to cancel?\n(Bean collection will be lost)"
                                delegate:self
                                cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
  [confirmCancel show];
}
/*****************************************************
 Delegates
 *****************************************************/
#pragma mark - UITextFieldDelegate methods
- (void) textFieldDidBeginEditing:(UITextField *)textField
{
  if (textField == self.beanCollectionNameTextField)
  {
    self.navigationItem.rightBarButtonItem = self.beanCollectionTextFieldDoneButton;
  }
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
  if (textField == self.beanCollectionNameTextField)
  {
    self.navigationItem.rightBarButtonItem = self.beanCollectionNextButton;
  }
}

//Purpose: When "return" is pressed the keyboard will go away and "change" event will be triggered.
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  //Return key makes the keyboard go away.
  if (textField == self.beanCollectionNameTextField)
  {
    [textField resignFirstResponder];
    return YES;
  }
  
  return NO;
}

#pragma mark - UITextViewDelegate methods
//Purpose: Catch the event when changes have been made to the bean collection's note.
- (void)textViewDidEndEditing:(UITextView *)textView
{
  if (textView == self.beanCollectionNoteTextView)
  {    
    //Change back to "Next" button
    self.navigationItem.rightBarButtonItem = self.beanCollectionNextButton;
  }
}

- (void) textViewDidBeginEditing:(UITextView *)textView
{
  if (textView == self.beanCollectionNoteTextView)
  {
    self.navigationItem.rightBarButtonItem = self.beanCollectionTextViewDoneButton;
  }

}

#pragma mark - UIAlertViewDelegate methods
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
  //User has confirmed cancellation? 
  if (buttonIndex == 0)
  {
    [self performSegueWithIdentifier:@"BeanCollectionInfoCancelSegue" sender:self];
  }
}

@end
