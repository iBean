//
//  BeanCollectionInfoViewController.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-05.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BeanCollection;

@interface BeanCollectionInfoViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) BeanCollection *beanCollection;
@property (nonatomic, assign) BOOL editMode;
@property (strong, nonatomic) UIBarButtonItem *beanCollectionTextViewDoneButton;
@property (strong, nonatomic) UIBarButtonItem *beanCollectionTextFieldDoneButton;

/* Utility methods */
- (void) initViewController;
- (void) initWithModeAndBeanCollection:(BOOL)editMode: (BeanCollection*) bc;

/* UI Outlets */
@property (weak, nonatomic) IBOutlet UITextField *beanCollectionNameTextField;
@property (weak, nonatomic) IBOutlet UITextView *beanCollectionNoteTextView;
@property (weak, nonatomic) IBOutlet UIScrollView *beanCollectionScrollView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *beanCollectionNextButton;
@property (weak, nonatomic) IBOutlet UIView *beanCollectionNoteTextViewShadowView;

/* UI Actions */
- (IBAction) cancelBeanCollection:(id)sender;

- (IBAction) keyboardDisplayed:(NSNotification*)keyboardNotification;
- (IBAction) keyboardHidden:(NSNotification*)keyboardNotification;

/* UITextFieldDelegate */
- (BOOL)textFieldShouldReturn:(UITextField *)textField;

/* UITextViewDelegate */
- (void)textViewDidEndEditing:(UITextView *)textView;

@end
