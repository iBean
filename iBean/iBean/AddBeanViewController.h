//
//  AddBeanViewController.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-09.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BeanCollection;

@interface AddBeanViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) BeanCollection *beanCollection;
@property (nonatomic, assign) BOOL editMode;

/* Utility methods */
- (void) initViewController;
- (void) initWithModeAndBeanCollection:(BOOL)editMode: (BeanCollection*) bc;

/* UI Outlets */
@property (weak, nonatomic) IBOutlet UITextField *beanNameTextField;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIStepper *amountStepper;
@property (weak, nonatomic) IBOutlet UILabel *grinderSettingLabel;
@property (weak, nonatomic) IBOutlet UIStepper *grinderSettingStepper;
@property (weak, nonatomic) IBOutlet UINavigationBar *addBeanNavigationBar;

/* UI Actions */
- (IBAction) amountStepperChanged:(id)sender;
- (IBAction) grinderSettingStepperChanged:(id)sender;
- (IBAction) cancelBean:(id)sender;
- (IBAction) commitBean:(id)sender;

@end
