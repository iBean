//
//  InstantExtractionViewController.m
//  iBean
//
//  Created by Eddie Ehlin on 2012-12-31.
//  Copyright (c) 2012 Eddie Ehlin. All rights reserved.
//
#import <AudioToolbox/AudioToolbox.h>
#import "InstantExtractionViewController.h"
#import "AppDelegate+Storage.h"

@interface InstantExtractionViewController ()

@end

@implementation InstantExtractionViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self initViewController];
}

- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  [self haltExtractionTimer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*****************************************************
                  Utility methods
 *****************************************************/
#pragma mark - Utility methods
- (void) initViewController
{
  //Fetch extraction timer setting
  NSNumber* extractionTimerSetting = [(AppDelegate*) [[UIApplication sharedApplication] delegate] getInstantExtractionTimer];
  [self.extractionSettingStepper setValue: extractionTimerSetting != nil ? [extractionTimerSetting doubleValue] : 0.0f];
  [self updateExtractionSettingLabel];
  
  //Allocate and init the array holding extraction times (in memory).
  if (self.extractionTimes == nil)
  {
    self.extractionTimes = [[NSMutableArray alloc] init];
  }
}

- (void) updateExtractionProgressLabel
{
  self.extractionProgress += 0.1f;
  self.extractionProgressLabel.text = [NSString stringWithFormat:@"%1.1f", self.extractionProgress];
  
  //We will only increment extractions iff extraction setting/roof is set to a value > 0
  if (self.extractionProgress >= self.extractionSettingStepper.value && self.extractionSettingStepper.value > 0)
  {
    [self haltExtractionTimer];
    
    UIAlertView *extractionCompleteAlert = [[UIAlertView alloc]
                                            initWithTitle:@"Coffee time!"
                                            message:@"Extraction completed."
                                            delegate:self
                                            cancelButtonTitle:@"OK" otherButtonTitles: nil];
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    [extractionCompleteAlert show];
  }
}

- (void) updateExtractionSettingLabel
{
  //If value is < 0 (change color to hint user that roof is disabled).
  if (self.extractionSettingStepper.value > 0)
    self.extractionSettingLabel.textColor = [UIColor blackColor];
  else
    self.extractionSettingLabel.textColor = [UIColor lightGrayColor];
  
  self.extractionSettingLabel.text = [NSString stringWithFormat:@"%1.1f", self.extractionSettingStepper.value];
}

- (void) haltExtractionTimer
{
  if ([(AppDelegate*) [[UIApplication sharedApplication] delegate] getTimer] != nil)
  {
    [(AppDelegate*) [[UIApplication sharedApplication] delegate] haltTimer:YES];
  }
}

- (void) timerInterrupted
{
  //Add extraction time to our array of extraction times (datasource for tableview)
  [self.extractionTimes insertObject:[NSString stringWithString:self.extractionProgressLabel.text] atIndex:0];
  [self.extractionTimesTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
  
  //Return UI to "Start extraction" state.
  [self.extractionStateLabel setText:@"Press to start"];
  [self.extractionSettingStepper setEnabled:YES];
  self.extractionInProgress = NO;
}

/*****************************************************
                    UI Actions
*****************************************************/
#pragma mark - IBActions
- (IBAction)startExtraction:(id)sender {

  if (([(AppDelegate*) [[UIApplication sharedApplication] delegate] getTimer] == nil) || self.extractionInProgress == NO)
  {
    self.extractionProgress = 0.0f;
    [self.extractionStateLabel setText:@"Press to stop"];
    [self.extractionSettingStepper setEnabled:NO];
    self.extractionInProgress = YES;
    
    [(AppDelegate*) [[UIApplication sharedApplication] delegate] createTimer:self :@selector(updateExtractionProgressLabel) :nil :YES];
  }
  else if (([(AppDelegate*) [[UIApplication sharedApplication] delegate] getTimer] != nil))
  {
    [self haltExtractionTimer];
  }
}

- (IBAction)setExtractionTimer:(id)sender {
  
  NSError* error = [(AppDelegate*) [[UIApplication sharedApplication] delegate] setInstantExtractionTimer:[NSNumber numberWithDouble:self.extractionSettingStepper.value]];
  if (error == nil)
    [self updateExtractionSettingLabel];
}

/*****************************************************
 Delegates
 *****************************************************/
#pragma mark - UIAlertView delegate
- (void) alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
  [(AppDelegate*) [[UIApplication sharedApplication] delegate] incrementExtractionCount];
}

#pragma mark - UITableView datasource & delegate
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return self.extractionTimes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"ExtractionTimeCell";
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  
  //Configure the cell and set its title
  cell.textLabel.text = [NSString stringWithFormat:@"%@ s", [self.extractionTimes objectAtIndex:indexPath.row]];
  cell.detailTextLabel.text = @"";
  return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
  // Return NO if you do not want the specified item to be editable (we want to be able to REMOVE though).
  return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (editingStyle == UITableViewCellEditingStyleDelete) {
    // Delete the row from the data source
    if (self.extractionTimes != nil)
    {
      [self.extractionTimes removeObjectAtIndex: indexPath.row];
    }
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
  }
  else if (editingStyle == UITableViewCellEditingStyleInsert) {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
  }
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
  // Return NO if you do not want the item to be re-orderable.
  return NO;
}

@end
