//
//  BeanCollectionBeanListViewController.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-05.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "BeanCollectionBeanListViewController.h"
#import "EditBeanViewController.h"
#import "AddBeanViewController.h"
#import "ExtractionViewController.h"
#import "BeanCollection.h"
#import "Bean.h"
#import "UIColor+Extra.h"

@interface BeanCollectionBeanListViewController ()

@end

@implementation BeanCollectionBeanListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self initViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  //AddBeanSegue
  if ([segue.identifier isEqualToString:@"AddBeanSegue"])
  {
    //Pass on the edit/add state and bean collection
    AddBeanViewController *beanViewController = segue.destinationViewController;
    [beanViewController initWithModeAndBeanCollection:self.editMode :self.beanCollection];
  }
  else if ([segue.identifier isEqualToString:@"EditBeanSegue"])
  {
    EditBeanViewController *beanViewController = segue.destinationViewController;
    [beanViewController initWithModeAndBeanCollectionAndBeanIndex:self.editMode :self.beanCollection :[self.beanListTableView indexPathForSelectedRow].row];
  }
  else if ([segue.identifier isEqualToString:@"BeanListExtractionSegue"])
  {
    ExtractionViewController *extractionviewController = segue.destinationViewController;
    [extractionviewController initWithModeAndBeanCollection:self.editMode :self.beanCollection];
  }
}

/*****************************************************
 Utility methods
 *****************************************************/
- (void) initViewController
{
  //Allocate edit's "Done" button
  self.beanListEditDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(toggleEditMode:)];
  
  //Load beans into the table view.
  [self.beanListTableView reloadData];
}

- (void) initWithModeAndBeanCollection:(BOOL)editMode: (BeanCollection*) bc
{
  self.editMode = editMode;
  self.beanCollection = bc;
}

/*****************************************************
 UI Actions
 *****************************************************/
#pragma mark - IBActions
- (void) toggleEditMode:(id)sender
{
  NSMutableArray *bottomToolbarItems = [NSMutableArray arrayWithArray:self.beanListBottomToolbar.items];
  if (self.beanListTableView.editing)
  {
    [bottomToolbarItems replaceObjectAtIndex:0 withObject:self.beanListEditButton];
  }
  else
  {
    [bottomToolbarItems replaceObjectAtIndex:0 withObject:self.beanListEditDoneButton];
  }
  [self.beanListTableView setEditing:!self.beanListTableView.editing animated:YES];
  [self.beanListBottomToolbar setItems:bottomToolbarItems animated:YES];
}

/*****************************************************
 UITableView related delegate / datasource methods
 *****************************************************/
#pragma mark - UITableViewDelegate methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return self.beanCollection.beans.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"BeanCell";
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  
  //Configure the cell and set its title
  Bean *currentBean = [self.beanCollection.beans objectAtIndex:indexPath.row];
  cell.textLabel.text = currentBean.name;
  
  //Set selected background color
  UIView *cellSelectedBackgroundView = [[UIView alloc] init];
  [cellSelectedBackgroundView setBackgroundColor:[UIColor colorFromHEX:[UIColor selectedCellBackgroundColor]]];
  [cell setSelectedBackgroundView:cellSelectedBackgroundView];
  
  return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
  // Return NO if you do not want the specified item to be editable (we want to be able to REMOVE though).
  return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (editingStyle == UITableViewCellEditingStyleDelete) {
    // Delete the row from the data source
    if (self.beanCollection != nil)
    {
      [self.beanCollection removeObjectFromBeansAtIndex: indexPath.row];
    }
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
  }
  else if (editingStyle == UITableViewCellEditingStyleInsert) {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
  }
}

 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }

 // Override to support rearranging the table view.
//http://developer.apple.com/library/ios/#documentation/userexperience/conceptual/tableview_iphone/ManageReorderRow/ManageReorderRow.html
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
   if (self.beanCollection != nil && fromIndexPath.row != toIndexPath.row)
   {
     if (toIndexPath.row < (self.beanCollection.beans.count - 1))
     {
       NSMutableIndexSet *beanIndexes = [NSMutableIndexSet indexSetWithIndex:fromIndexPath.row];
       [beanIndexes addIndex:toIndexPath.row];
       NSArray *beans = [NSArray arrayWithObjects:[self.beanCollection.beans objectAtIndex:fromIndexPath.row], [self.beanCollection.beans objectAtIndex:toIndexPath.row], nil];
       [self.beanCollection replaceBeansAtIndexes:beanIndexes withBeans:beans];
     }
   }
 }

@end
