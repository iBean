//
//  ThresholdListViewController.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-27.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "ThresholdListViewController.h"
#import "ThresholdViewController.h"
#import "Configuration+Interface.h"
#import "ThresholdCell.h"
#import "AppDelegate+Storage.h"
#import "UIColor+Extra.h"

@interface ThresholdListViewController ()

@end

@implementation ThresholdListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self initViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"ThresholdsAddNewThresholdSegue"] || [segue.identifier isEqualToString:@"ThresholdsTopAddNewThresholdSegue"])
  {
    ThresholdViewController* thresholdViewController = segue.destinationViewController;
    [thresholdViewController initWithConfigurationAndThresholdIndex:self.configuration : -1];
  }
  else if ([segue.identifier isEqualToString:@"ThresholdsEditThresholdSegue"])
  {
    ThresholdViewController* thresholdViewController = segue.destinationViewController;
    [thresholdViewController initWithConfigurationAndThresholdIndex:self.configuration :self.thresholdListTableView.indexPathForSelectedRow.row];
  }
}

/*****************************************************
 Utility methods
 *****************************************************/
- (void) initViewController
{
  [self.thresholdListTableView reloadData];
  self.thresholdListEditDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(toggleEditMode:)];
}

- (void) initWithConfiguration:(Configuration *)configuration
{
  self.configuration = configuration;
}

/*****************************************************
 UI Actions
 *****************************************************/
#pragma mark - IBActions
- (void) toggleEditMode:(id)sender
{
  NSMutableArray *bottomToolbarItems = [NSMutableArray arrayWithArray:self.thresholdListBottomToolbar.items];
  if (self.thresholdListTableView.editing)
  {
    [bottomToolbarItems replaceObjectAtIndex:0 withObject:self.thresholdListEditButton];
  }
  else
  {
    [bottomToolbarItems replaceObjectAtIndex:0 withObject:self.thresholdListEditDoneButton];
  }
  [self.thresholdListTableView setEditing:!self.thresholdListTableView.editing animated:YES];
  [self.thresholdListBottomToolbar setItems:bottomToolbarItems animated:YES];
}

/*****************************************************
 Delegates
 *****************************************************/
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  // Return the number of sections.
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  // Return the number of rows in the section.
  return self.configuration.thresholds.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"ThresholdCell";
  ThresholdCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  
  // Configure the cell...
  [cell initWithThresholdEntity:[self.configuration.thresholds objectAtIndex:indexPath.row]];
  
  //Set selected background color
  UIView *cellSelectedBackgroundView = [[UIView alloc] init];
  [cellSelectedBackgroundView setBackgroundColor:[UIColor colorFromHEX:[UIColor selectedCellBackgroundColor]]];
  [cell setSelectedBackgroundView:cellSelectedBackgroundView];
  
  return cell;
}

#pragma mark - Table view delegate

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
  // Return NO if you do not want the specified item to be editable (we want to be able to REMOVE though).
  return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (editingStyle == UITableViewCellEditingStyleDelete) {
    // Delete the row from the data source
    if (self.configuration != nil)
    {
      [self.configuration removeObjectFromThresholdsAtIndex: indexPath.row];
    }
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    //Commit change!
    if ([(AppDelegate*) [[UIApplication sharedApplication] delegate] save] != nil)
    {
      UIAlertView *deleteRowAlert = [[UIAlertView alloc] initWithTitle:@"Configuration error!" message:@"iBean was unable to delete threshold!\nPlease try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
      [deleteRowAlert show];
      
      //Rollback and reload table view
      [(AppDelegate*) [[UIApplication sharedApplication] delegate] rollback];
      [self.thresholdListTableView reloadData];
    }
    
  }
  else if (editingStyle == UITableViewCellEditingStyleInsert) {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
  }
}


@end
