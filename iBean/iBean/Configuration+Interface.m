//
//  Configuration+Interface.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-14.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "Configuration+Interface.h"

@implementation Configuration (Interface)

- (void) addThresholdsObject:(Threshold *)t
{
  NSMutableOrderedSet *tmpSet = [self mutableOrderedSetValueForKey:@"thresholds"];
  [tmpSet addObject:t];
}

- (void) removeObjectFromThresholdsAtIndex:(NSUInteger)thresholdIndex
{
  NSMutableOrderedSet *tmpSet = [self mutableOrderedSetValueForKey:@"thresholds"];
  if (thresholdIndex >= 0 && thresholdIndex < tmpSet.count)
    [tmpSet removeObjectAtIndex:thresholdIndex];
  else
    NSLog(@"Configuration+Interface - removeObjectFromThresholdsAtIndex: Index out of range!");
}

- (void)replaceThresholdsAtIndexes:(NSIndexSet *)thresholdsIndexes withThresholds:(NSArray *)thresholds
{
  if (thresholds.count > 0)
  {
    NSMutableOrderedSet *tmpSet = [self mutableOrderedSetValueForKey:@"thresholds"];
    [tmpSet replaceObjectsAtIndexes:thresholdsIndexes withObjects:thresholds];
  }
}

@end
