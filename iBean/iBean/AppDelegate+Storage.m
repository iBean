//
//  AppDelegate+Storage.m
//  iBean
//
//  Created by Eddie Ehlin on 2012-12-31.
//  Copyright (c) 2012 Eddie Ehlin. All rights reserved.
//

#import "AppDelegate+Storage.h"
#import "Configuration+Interface.h"
#import "BeanCollection.h"
#include "Threshold.h"

@implementation AppDelegate (Storage)

#pragma mark - Common storage related methods

- (NSError*) save
{
  NSError *error = nil;
  [self.managedObjectContext save:&error];
  return error;
}

- (NSError*) deleteObject: (NSManagedObject*) managedObject
{
  NSError *error = nil;
  if (managedObject != nil && self.managedObjectContext != nil)
  {
    [self.managedObjectContext deleteObject:managedObject];
    error = [self save];
  }
  return error;
}

- (void) rollback
{
  if (self.managedObjectContext != nil)
  {
    [self.managedObjectContext rollback];
  }
}

#pragma mark - iBean configuration related methods
- (Configuration*) getConfiguration
{
  Configuration* config = nil;
  if (self.managedObjectContext)
  {
    //Note: There should only be ONE entity of this type.
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Configuration"];
    [fetchRequest setFetchLimit:1];
    
    NSError *fetchRequestError = nil;
    NSArray *fetchRequestResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchRequestError];
    
    if (fetchRequestResult == nil || fetchRequestError != nil)
    {
      NSLog(@"getConfiguration - Fetch request resulted in an error!");
      
      UIAlertView *loadErrorAlert = [[UIAlertView alloc]
                                     initWithTitle:@"Configuration error!"
                                     message:@"Unable to load iBean configuration."
                                     delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
      [loadErrorAlert show];
    }
    else if (fetchRequestResult.count == 0)
    {
      config = [NSEntityDescription insertNewObjectForEntityForName:@"Configuration" inManagedObjectContext:self.managedObjectContext];
      if (config != nil)
      {
        //Set default configuration values:
        config.instantExtractionTimer = [NSNumber numberWithDouble:0.0f];
      }
      else
      {
        UIAlertView *createConfigurationAlert = [[UIAlertView alloc]
                                                 initWithTitle:@"Configuration error!"
                                                 message:@"iBean unable to create configuration object.\n(Please check memory available.)"
                                                 delegate:nil
                                                 cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [createConfigurationAlert show];
      }
    }
    else
    {
      config = [fetchRequestResult objectAtIndex:0];
    }
  }
  return config;
}

//Extraction count related methods
- (NSError*) setCountExtractions: (BOOL) countExtractions
{
  NSError *error = nil;
  Configuration *c = [self getConfiguration];
  if (c != nil)
  {
    c.countExtractions = [NSNumber numberWithBool:countExtractions];
    error = [self save];
  }
  
  return error;
}

- (NSNumber*) getExtractionCount
{
  NSNumber *extractionCount = nil;
  Configuration* c = [self getConfiguration];
  extractionCount = c != nil ? c.extractionCount : [NSNumber numberWithInt:-1];
  return extractionCount;
}

- (NSError*) setExtractionCount: (NSNumber*) extractionCount
{
  NSError *error = nil;
  Configuration *c = [self getConfiguration];
  if (c != nil)
  {
    c.extractionCount = extractionCount < 0 ? 0 : extractionCount;
    error = [self save];
    
    if (error != nil)
    {
      UIAlertView *setExtractionCountAlert = [[UIAlertView alloc]
                                              initWithTitle:@"Configuration error!"
                                              message:@"iBean unable to set extraction count value."
                                              delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles: nil];
      [setExtractionCountAlert show];
    }
    else
    {
      [self processThresholds];
    }
  }
  
  return error;
}

- (NSNumber*) incrementExtractionCount
{
  Configuration *c = [self getConfiguration];
  
  if (c != nil)
  {
    if ([c.countExtractions boolValue] == YES)
    {
      if ([self setExtractionCount:[NSNumber numberWithInteger:([c.extractionCount integerValue] + 1)]] == nil)
      {
        return c.extractionCount;
      }
    }
  }
  return [NSNumber numberWithInteger:-1];
}

- (void) processThresholds
{
  //First out, lets sort the thresholds
  Configuration *configuration = [self getConfiguration];
  
  if (configuration != nil)
  {
    if ([configuration.useThresholds boolValue] == YES)
    {
      self.activeThresholdsToProcess = [[NSMutableArray alloc] init];
      NSArray *sortedThresholds = [configuration.thresholds sortedArrayUsingComparator:
                                   ^NSComparisonResult(id obj1, id obj2)
                                   {
                                     Threshold* t1 = obj1;
                                     Threshold* t2 = obj2;
                                     
                                     if ([[t1 value] integerValue] > [[t2 value] integerValue])
                                     {
                                       return (NSComparisonResult)NSOrderedDescending;
                                     }
                                     
                                     if ([[t1 value] integerValue] < [[t2 value] integerValue])
                                     {
                                       return (NSComparisonResult)NSOrderedAscending;
                                     }
                                     
                                     return (NSComparisonResult)NSOrderedSame;
                                   }];
      
      //Time to go through all thresholds and se which one(s) that are hit!
      for (Threshold *t in sortedThresholds)
      {
        //Case 1 - Threshold is hit & is set to be recurring
        if (([configuration.extractionCount integerValue] % [t.value integerValue]) == 0 && [t.recurring boolValue] == YES)
        {
          //Enable/re-enable the threshold.
          t.enabled = [NSNumber numberWithBool:YES];
        }
        
        //Case 2 - Threshold is hit & enabled
        if ([configuration.extractionCount integerValue] >= [t.value integerValue] && [t.enabled boolValue] == YES)
        {
          [self.activeThresholdsToProcess addObject:t];
        }
      }
      
      //Save context (to make sure that altered thresholds gets saved).
      [self saveContext];
      
      //Now, let's go through all active thresholds.
      [self processActiveThreshold];
    }
  }
}

- (void) processActiveThreshold
{
  if (self.activeThresholdsToProcess != nil)
  {
    if (self.activeThresholdsToProcess.count > 0)
    {
      Threshold *t = [self.activeThresholdsToProcess objectAtIndex:0];
      [self.activeThresholdsToProcess removeObject:t];
      UIAlertView *thresholdAlert = [[UIAlertView alloc] initWithTitle:@"Threshold reached!" message:t.name delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
      [thresholdAlert show];
    }
    else
    {
      self.activeThresholdsToProcess = nil;
    }
  }
}

- (Threshold*) createThreshold
{
  Threshold *t = nil;
  
  if (self.managedObjectContext != nil)
  {
    t = [NSEntityDescription insertNewObjectForEntityForName:@"Threshold" inManagedObjectContext:self.managedObjectContext];
    
    //Set default values
    if (t != nil)
    {
      //TODO: Set default values here...
    }
  }
  return t;
}

#pragma mark - Instant extraction related methods (using Configuraiton)
- (NSError*)setInstantExtractionTimer:(NSNumber *)extractionTimer
{
  NSError *error = nil;
  
  Configuration* c = [self getConfiguration];
  if (c != nil)
  {
    c.instantExtractionTimer = extractionTimer;
    error = [self save];
  }
  
  if (error != nil)
  {
    UIAlertView *setInstantExtractionTimerAlert = [[UIAlertView alloc]
                                   initWithTitle:@"Configuration error!"
                                   message:@"iBean unable to set extraction timer setting for instant extraction."
                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [setInstantExtractionTimerAlert show];
  }
  
  return error;  
}

- (NSNumber*) getInstantExtractionTimer
{
  NSNumber* extractionTimerSetting = nil;
  Configuration* c = [self getConfiguration];
  if (c != nil)
  {
    extractionTimerSetting = c.instantExtractionTimer;
  }
  return extractionTimerSetting;
}


#pragma mark - iBean (Bean Collections) related storage methods
- (NSArray*) getBeanCollections
{
  NSArray *fetchRequestResult = nil;
  
  if (self.managedObjectContext != nil)
  {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"BeanCollection"];
    //For now we sort them on name.
    fetchRequest.sortDescriptors = [NSArray arrayWithObject: [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    
    NSError *fetchRequestError = nil;
    fetchRequestResult = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchRequestError];
    
    if (fetchRequestResult == nil || fetchRequestError != nil)
    {
      NSLog(@"getBeanCollections - Fetch request resulted in an error!");
      UIAlertView *loadErrorAlert = [[UIAlertView alloc]
                                     initWithTitle:@"Load error!"
                                     message:@"Unable to load bean collections."
                                     delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
      [loadErrorAlert show];
    }
  }
  
  return fetchRequestResult;
}

- (BeanCollection*) createBeanCollection
{
  BeanCollection* bc = nil;
  
  if (self.managedObjectContext != nil)
  {
    bc = [NSEntityDescription insertNewObjectForEntityForName:@"BeanCollection" inManagedObjectContext:self.managedObjectContext];
    
    //Set default values
    if (bc != nil)
    {
      bc.created = [NSDate date];
      bc.extractionTime = [NSNumber numberWithDouble:25.0f];
    }
  }
  
  return bc;
}

- (Bean*) createBean
{
  Bean *b = nil;
  
  if (self.managedObjectContext != nil)
  {
    b = [NSEntityDescription insertNewObjectForEntityForName:@"Bean" inManagedObjectContext:self.managedObjectContext];
    
    //Set default values
    if (b != nil)
    {
      //TODO: Set default values here...
    }
  }
  return b;
}

#pragma mark - iBean timer related methods
- (NSTimer*) getTimer
{
  return self.timer;
}

- (NSTimer*) createTimer:(id) callbackTarget: (SEL) selector: (id)userInfo: (BOOL)repeats
{
  if (self.timer != nil)
  {
    BOOL interruptCallbackTarget = self.timerCallbackTarget != callbackTarget;
    [self haltTimer: interruptCallbackTarget];
  }
  
  self.timerCallbackTarget = callbackTarget;
  self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:callbackTarget selector:selector userInfo:userInfo repeats:repeats];
  
  return self.timer;
}


- (void) haltTimer: (BOOL) interrupt
{
  if (self.timer != nil)
  {
    [self.timer invalidate];
    self.timer = nil;
    
    if (self.timerCallbackTarget != nil && interrupt == YES)
    {
      //Must call the target to notify that it's timer has been interrupted...
      //First we must make sure that target has implemented timerInterrupted method.
      if ([self.timerCallbackTarget respondsToSelector:@selector(timerInterrupted)])
      {
        [self.timerCallbackTarget performSelector:@selector(timerInterrupted)];
      }
    }
  }
}

#pragma mark - UIAlertView delegate
- (void) alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
  if (self.activeThresholdsToProcess != nil)
  {
    [self processActiveThreshold];
  }
}


@end
