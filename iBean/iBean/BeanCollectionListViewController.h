//
//  BeanCollectionListViewController.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-03.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BeanCollection;
@interface BeanCollectionListViewController : UITableViewController

@property (nonatomic, strong) NSArray *beanCollections;
@property (nonatomic, strong) BeanCollection *beanCollection;
@property (nonatomic, strong) UIBarButtonItem *beanCollectionsListEditDoneButton;

/* Utility methods */
- (void) initViewController;

/* UI Outlets */
@property (strong, nonatomic) IBOutlet UIBarButtonItem *beanCollectionsListEditButton;


/* UI Actions */
- (IBAction) toggleEditMode: (id)sender;

@end
