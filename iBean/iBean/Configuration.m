//
//  Configuration.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-27.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "Configuration.h"
#import "Threshold.h"


@implementation Configuration

@dynamic instantExtractionTimer;
@dynamic extractionCount;
@dynamic countExtractions;
@dynamic useThresholds;
@dynamic thresholds;

@end
