//
//  Threshold.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-02-17.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Configuration;

@interface Threshold : NSManagedObject

@property (nonatomic, retain) NSNumber * enabled;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * value;
@property (nonatomic, retain) NSNumber * recurring;
@property (nonatomic, retain) Configuration *configuration;

@end
