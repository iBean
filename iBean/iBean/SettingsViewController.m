//
//  SettingsViewController.m
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-27.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import "SettingsViewController.h"
#import "Configuration+Interface.h"
#import "AppDelegate+Storage.h"
#import "ThresholdListViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self initViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"SettingsThresholdSegue"])
  {
    ThresholdListViewController *thresholdListViewController = segue.destinationViewController;
    [thresholdListViewController initWithConfiguration:self.configuration];
  }
}

/*****************************************************
 Utility methods
 *****************************************************/
- (void) initViewController
{  
  //Let's go fetch Configuration!
  self.configuration = [(AppDelegate*) [[UIApplication sharedApplication] delegate] getConfiguration];
  
  if (self.configuration != nil)
  {
    self.countExtractionsSwitch.on = [self.configuration.countExtractions boolValue];
    self.useThresholdsSwitch.on = [self.configuration.useThresholds boolValue];
    
    self.extractionCountLabel.text = [self.configuration.extractionCount stringValue];
    self.extractionCountStepper.value = [self.configuration.extractionCount integerValue];
  }
  else
  {
    self.countExtractionsSwitch.enabled = NO;
    self.useThresholdsSwitch.enabled = NO;
    self.extractionCountStepper.enabled = NO;
  }
}

/*****************************************************
 UI Actions
 *****************************************************/
- (void) countExtractionsSwitchChanged:(id)sender
{
  if (self.configuration != nil)
  {
    self.configuration.countExtractions = [NSNumber numberWithBool: self.countExtractionsSwitch.on];
    NSError *error = [(AppDelegate*) [[UIApplication sharedApplication] delegate] save];
    if (error != nil)
    {
      UIAlertView *setCountExtractionsAlert = [[UIAlertView alloc]
                                               initWithTitle:@"Configuration error!"
                                               message:@"iBean was not able to save configuration attribute for \"Count extractions\"."
                                               delegate:nil
                                               cancelButtonTitle:@"OK" otherButtonTitles: nil];
      [setCountExtractionsAlert show];
    }
  }
}

- (void) useThresholdsSwitchChanged:(id)sender
{
  if (self.configuration != nil)
  {
    self.configuration.useThresholds = [NSNumber numberWithBool: self.useThresholdsSwitch.on];
    NSError *error = [(AppDelegate*) [[UIApplication sharedApplication] delegate] save];
    if (error != nil)
    {
      UIAlertView *setUseThresholdsAlert = [[UIAlertView alloc]
                                               initWithTitle:@"Configuration error!"
                                               message:@"iBean was not able to save configuration attribute for \"Thresholds\"."
                                               delegate:nil
                                               cancelButtonTitle:@"OK" otherButtonTitles: nil];
      [setUseThresholdsAlert show];
    }
  }
}

- (void) extractionCountStepperChanged:(id)sender
{
  if (self.configuration != nil)
  {
    NSError *error = [(AppDelegate*) [[UIApplication sharedApplication] delegate] setExtractionCount:[NSNumber numberWithDouble:self.extractionCountStepper.value]];
    self.extractionCountLabel.text = [self.configuration.extractionCount stringValue];
    if (error != nil)
    {
      UIAlertView *setExtractionCountAlert = [[UIAlertView alloc]
                                               initWithTitle:@"Configuration error!"
                                               message:@"iBean was not able to save configuration attribute for \"Extraction count\"."
                                               delegate:nil
                                               cancelButtonTitle:@"OK" otherButtonTitles: nil];
      [setExtractionCountAlert show];
    }
    else
    {
      self.extractionCountLabel.text = [self.configuration.extractionCount stringValue];
    }
  }
}

- (void) resetExtractionCount:(id)sender
{
  UIAlertView *resetConfirmAlert = [[UIAlertView alloc]
                                    initWithTitle:@"Reset extraction count?"
                                    message:@"Are you sure about resetting extraction count?"
                                    delegate:self
                                    cancelButtonTitle:@"Yes"
                                    otherButtonTitles:@"No", nil];
  [resetConfirmAlert show];
}

/*****************************************************
 Delegates
 *****************************************************/
#pragma mark - UIAlertView delegate
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
  if (buttonIndex == 0)
  {
    if (self.configuration != nil)
    {
      NSError *error = [(AppDelegate*) [[UIApplication sharedApplication] delegate] setExtractionCount:[NSNumber numberWithInt:0]];
      self.extractionCountLabel.text = [self.configuration.extractionCount stringValue];
      self.extractionCountStepper.value = [self.configuration.extractionCount doubleValue];
      if (error != nil)
      {
        UIAlertView *setExtractionCountAlert = [[UIAlertView alloc]
                                                initWithTitle:@"Configuration error!"
                                                message:@"iBean was not able to save configuration attribute for \"Extraction count\"."
                                                delegate:nil
                                                cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [setExtractionCountAlert show];
      }
      else
      {
        self.extractionCountLabel.text = [self.configuration.extractionCount stringValue];
        self.extractionCountStepper.value = [self.configuration.extractionCount doubleValue];
      }
    }
  }
}
@end
