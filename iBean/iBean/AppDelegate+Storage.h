//
//  AppDelegate+Storage.h
//  iBean
//
//  Created by Eddie Ehlin on 2012-12-31.
//  Copyright (c) 2012 Eddie Ehlin. All rights reserved.
//

#import "AppDelegate.h"
@class Configuration;
@class BeanCollection;
@class Bean;
@class Threshold;

/*
 Main purpose of this category is to have all storage (core data) related
 methods etc. in one area. Just to keep it clean.
 */

@interface AppDelegate (Storage) <UIAlertViewDelegate>

#pragma mark - Common storage related methods
- (NSError*) save;
- (NSError*) deleteObject: (NSManagedObject*) managedObject;
- (void) rollback;


#pragma mark - iBean configuration related methods
- (Configuration*) getConfiguration;

//Extraction counter methods
- (NSError*) setCountExtractions: (BOOL) countExtractions;
- (NSNumber*) getExtractionCount;
- (NSError*) setExtractionCount: (NSNumber*) extractionCount;
- (NSNumber*) incrementExtractionCount;

//Thresholds methods
- (void) processThresholds;
- (void) processActiveThreshold;
- (Threshold*) createThreshold;



//Instant extraction related methods
- (NSError*) setInstantExtractionTimer: (NSNumber*) extractionTimer;
- (NSNumber*) getInstantExtractionTimer;



#pragma mark - iBean related storage methods
- (NSArray*) getBeanCollections;
- (BeanCollection*) createBeanCollection;
- (Bean*) createBean;

#pragma mark - iBean timer methods
- (NSTimer*) getTimer;
- (NSTimer*) createTimer:(id) callbackTarget: (SEL) selector: (id)userInfo: (BOOL)repeats;
- (void) haltTimer: (BOOL)interrupt;

@end
