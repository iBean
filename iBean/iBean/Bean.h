//
//  Bean.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-10.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BeanCollection;

@interface Bean : NSManagedObject

@property (nonatomic, retain) NSNumber * amount;
@property (nonatomic, retain) NSNumber * grindSetting;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) BeanCollection *beanCollection;

@end
