//
//  ThresholdCell.h
//  iBean
//
//  Created by Eddie Ehlin on 2013-01-27.
//  Copyright (c) 2013 Eddie Ehlin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Threshold;

@interface ThresholdCell : UITableViewCell

@property (strong, nonatomic) Threshold* threshold;

@property (weak, nonatomic) IBOutlet UISwitch *enabledSwitch;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;

/* Utility methods */
- (void) initWithThresholdEntity: (Threshold*) threshold;

/* UI Actions */
- (IBAction)switchChanged:(id)sender;

@end
