# iBean #

![Logo](iBean/ibean-144-144.png "Logo")


## Legacy notes ##

The application was created out of curiosity. I wanted to test the iOS platform and also Objective-C which both were new to me at the time. I also remember like 60% into the project there was a new pool-based memory management introduced (or pushed towards really), and thus the iBean.old folder in the repo.

I used the app myself for many years and so did my wife as well, so in retrospective it was an interesting and good journey to take.

After some years I decided to take the application down since I no longer had any interest in paying the Appstore fee, but the application had spread quite a bit and doing so all by its own.

**Stats before taking it down (most frequent downloads at top):**
- Australia
- USA
- Sweden
- China
- Canada
- UK
- New Zealand
- Japan
- France
- Germany
- Netherlands
- Russia
- Spain
- Hong Kong
- Austria
- Denmark
- Italy
- Macedonia
- Turkey
- Indonesia
- Malaysia
- Taiwan
- Thailand
- Guatamala
- Mexico

*Most importantly: I don't remember receiving a single crash report, so it must have been rock solid. :-)*

## Some information about the application (mostly derived from old posts) ##

**In short:** An app that enables you to keep track of your extractions and lets you focus on the harder variables such as pressure, temperature etc.

A simple smartphone/tablet app which enabled the user to keep track of some of the most important variables (or at least the most easy ones to control) for extracting espresso. Focus for the app, design-wise, was to keep it functional and easy following the KISS (Keep It Simple Stupid) principle.

### iBean allowed the user to; ###

- Create bean recipes/blends each holding information like;
  + Extraction time
  + Coffee bean(s) - a recipe can contain several beans each having its own grind setting and amount
  + Notes
- Experimenting with extraction time using a dedicated timer functionality
- Keep track of number of extractions (for fun or for triggering so called Thresholds)
- Create custom Thresholds that are triggered and notifies user when the number of extractions hits (or exceeds) the threshold's value - easy way to remember maintenence tasks
  + Example thresholds:
  + Threshold named "Empty drip tray" having a threshold-value of X ((re)enabled every Xth extraction - recurring mode)
  + Change waterfilter every 300th extraction
  + Buy more beans (recurring every 150 extractions)

### Backround & requirements ###

The application was constructed from the requirements I had been collecting from my years of espresso extraction at home.

### Screenshots ###

![Screenshot for iPhone 4](Screenshots/iPhone%204%20inch%20retina/iOS%20Simulator%20Screen%20shot%2025%20mar%202013%2022.06.04.png "Screenshot for iPhone 4")

[More screenshots.](Screenshots/ "Link to Screenshot directory")
